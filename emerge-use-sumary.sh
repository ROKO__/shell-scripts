#!/bin/sh -f
 # Copyright 2007 Jose San Leandro Armendariz
 # Distributed under the terms of the GNU General Public License
 
 # functions
 usage () {
   cat << USAGE

 emerge [..] | $0

# Displays the emerge output describing each USE flag.

 USAGE
   exit 1;
 }

 while read line;
 do
   if [ "x${line}" != "x" ]; then
     valid=`echo ${line} | grep " USE="`
     if [ "x${valid}" != "x" ]; then
       pkg=`echo ${line} | tr -s " " "\n" | egrep -v "^$" | grep "/"`
       allUses=`echo ${line} | awk -F"\"" '{print $2;}' | tr " " "\n" | grep -v "(" | grep -v "%"`
       echo ${pkg} USE=\"${allUses}\"
       if [ "x${allUses}" != "x" ]; then
         for use in ${allUses}; do
           use=`echo ${use} | tr -d "-"`
           for file in "/usr/portage/profiles/use.desc" "/usr/portage/profiles/use.local.desc"; do
               descriptions=`egrep "^${use} -" ${file} | egrep "^${pkg} -" | grep -v ":"`
               if [ "x${descriptions}" == "x" ]; then
                   descriptions=`egrep "^${use} -" ${file} | head -n 1`
               fi
               if [ "x${descriptions}" != "x" ]; then
                   break;
               fi
           done
           if [ "x${descriptions}" != "x" ]; then
             echo "${descriptions}" | sed "s|${pkg}||g" | sed "s_ -_:_" | \
             awk -F"-" '{printf("  %s\n", $0);}'
           fi
         done
       fi
     fi
   fi
 done
