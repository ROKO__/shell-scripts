#!/bin/bash
# Rosen Aleksandrov
# rosen.aleksandrov.1988@mail.ru
#Run fstrim
LOG=/var/log/trim.log
fstrim -v --all >> $/var/log/trim.log
echo "Time: $(date)">>$LOG
exit 0
