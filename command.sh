#!/bin/bash
# check if centos httpd or debian/ubuntu apache2ctl

CMD=`find /bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin -name httpd`
if [ -z "$CMD" ];
then
echo "Debian/Ubuntu apache2ctl found" && CMD="apache2ctl"
else
echo "Centos/Fedora/SUSE/RHEL httpd found" && CMD="httpd"
fi

 # add rest of the script as we found all bins in $PATH
echo -e "Working...\n"

"$CMD" -D DUMP_VHOSTS
#"$CMDS" -D DUMP_VHOSTS | egrep "'default server'|alias" | sed "s/ /\n/g" | sed "s/alias//" | sed '/^$/d'

