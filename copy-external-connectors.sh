#!/bin/bash
# Author: Rosen Aleksandrov
# rosen.aleksandrov@opencode.com

# Colors
BLUE='\033[0;34m'
GREEN='\033[0;32m'
CYAN='\033[0;36m'
YELLOW='\033[1;33m'
RED='\033[1;31m'
NC='\033[0m' # No Color

# Check if dialog is installed
pkg="dialog"
if rpm -q $pkg
then
    echo
    printf "$GREEN $pkg installed, continue...\n"
    else
    printf "$CYAN installing $pkg ...\n" ; dnf -y install $pkg
    echo
fi

# External connectors destination directory
dest='/aux0/customer/connectors/external'

# Dialog box
result=$(dialog --inputbox "Choose directory with external connectors" 0 0 2>&1 1>/dev/tty);

# Copy external connectors
echo
printf "$YELLOW copy all external connectors from $result to /aux0/customer/connectors/external\n" ; cp -rv $result/*.xml $dest
echo
printf "$BLUE occonnectors reload..\n" ; find /aux0/customer/connectors/external -iname "*.xml" -mtime -1 -exec occonnectors reload {} \;
echo
printf "$RED Please execute occonfman reload.\n"
