#!/bin/bash
# https://github.com/mantinband/best-vim-config-ever
# Only RHEL8/CentOS8 and derivatives
# This script will replace default vim 8.0 with 8.2 + couple of extras
# Author of this script: Rosen Aleksandrov <rosen@aleksandrov.tech>

echo -e "\033[33mSet default python to 3.X\033[m"
alternatives --set python /usr/bin/python3

echo -e "\033[33mChek if vim is installed and uninstall it.\033[m"
vimarr=(vim-enhanced vim-common vim-filesystem)
for v in  ${vimarr[*]}
 do
  installed=$(rpm -q $v)
  if [ !  "$viminstalled" == "package $v is not installed" ];
   then
    echo -e "\033[32m$v is not installed!\033[m"
  else
    echo -e "\033[31mPackage  $v is installed, uninstalling!\033[m"
	yum remove $v -y
  fi
done


echo -e "\033[33mCheck if necessary packages are installed\033[m"
pckarr=(cmake git python36 python36-devel gcc make ncurses ncurses-devel ctags git tcl-devel ruby ruby-devel lua lua-devel luajit luajit-devel perl perl-devel)
for i in  ${pckarr[*]}
 do
  isinstalled=$(rpm -q $i)
  if [ !  "$isinstalled" == "package $i is not installed" ];
   then
    echo -e "\033[32mPackage  $i already installed!\033[m"
  else
    echo -e "\033[31m$i is not installed! Installing $i\033[m"
    yum install $i -y
  fi
done

echo -e "\033[36mBuild vim 8.1+ from source.\033[m"
cd /opt && git clone https://github.com/vim/vim.git
cd vim/src && ./configure --with-features=huge --enable-multibyte --enable-python3interp=yes --with-python3-config-dir=$(python3-config --configdir) --enable-cscope && make -j$((`nproc`+1)) && make install && hash -r


echo -e "\033[36mInstalling vim configuration.\033[m"
cd ~ ; curl -O https://raw.githubusercontent.com/mantinband/best-vim-config-ever/master/.vimrc

echo -e "\033[36mInstalling necessary plugins.\033[m"
mkdir -p ~/.vim/autoload ~/.vim/bundle && curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
cd ~/.vim/bundle && git clone git://github.com/chase/vim-ansible-yaml.git
cd ~/.vim/bundle && git clone https://github.com/kien/ctrlp.vim
cd ~/.vim/bundle && git clone https://github.com/tpope/vim-fugitive
git clone https://github.com/vim-airline/vim-airline ~/.vim/bundle/vim-airline
cd ~/.vim/bundle && git clone https://github.com/google/vim-searchindex.git
git clone https://github.com/powerline/fonts.git --depth=1
cd fonts
./install.sh
cd ..
rm -rf fonts
fc-cache -vf
mkdir ~/.vim/undo
cd ~/.vim/bundle && git clone https://github.com/Valloric/YouCompleteMe
cd YouCompleteMe && git submodule update --init --recursive && python ./install.py
cd ~/.vim/bundle && git clone https://github.com/yegappan/grep
