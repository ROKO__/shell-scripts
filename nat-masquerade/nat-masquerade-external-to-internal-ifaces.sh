#!/bin/bash
# Script for nat masquerade between external and internal interfaces.
# Debian only at the moment
# Author: Rosen Aleksandrov
# rosen@aleksandrov.tech


currentDir="$( dirname "$( command -v "$0" )" )"
scriptsCommonUtilities="$( dirname "$currentDir" )/scripts-common/utilities.sh"
[ ! -f "$scriptsCommonUtilities" ] && echo -e "ERROR: scripts-common utilities not found, you must install it before using this script (checked path: $scriptsCommonUtilities)" >&2 && exit 1
# shellcheck disable=1090
. "$scriptsCommonUtilities"

if ! isRootUser; then
  errorMessage "This script must be launched with root user."
fi

warning "Debian only supported at the moment. Ubuntu support not guaranteed, you're warned!"

checkBin netfilter-persistent || errorMessage "This tool requires netfilter-persistent. Install it please, and then run this tool again."

forwarding=$(sysctl net.ipv4.ip_forward | grep -o '[0-1]\+')

if [[ ${forwarding} == 0 ]]; then
    warning "ip forwarding currently is not enabled!"
    echo
elif [[ $forwarding ]]; then
    writeMessage "ip forwarding is currently enabled!"
    echo
else
    errorMessage "Terminating..."
fi

writeMessage "Applying masquerade iptables settings..."
echo

echo -e "Please select external NIC"
read external

echo -e "Please select internal NIC"
read internal

iptables -A FORWARD -i ${internal} -o ${external} -j ACCEPT
iptables -A FORWARD -i ${external} -o ${internal} -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -t nat -A POSTROUTING -o ${external} -j MASQUERADE

writeMessage "Save iptables rules..."
iptables-save > /etc/iptables/rules.v4
ip6tables-save > /etc/iptables/rules.v6

writeMessage "Activate iptables rules..."
systemctl restart netfilter-persistent
