curl ifconfig.me
curl icanhazip.com
curl ipecho.net/plain
curl ifconfig.co
curl https://ipinfo.io/ip
curl https://ipecho.net/plain ; echo
curl http://smart-ip.net/myip

alias wanip='dig @resolver1.opendns.com ANY myip.opendns.com +short'
dig @ns1-1.akamaitech.net ANY whoami.akamai.net +short
alias wanip4='dig @resolver1.opendns.com A myip.opendns.com +short -4'

dig +short myip.opendns.com @resolver1.opendns.com
