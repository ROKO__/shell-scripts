#!/bin/bash

echo
echo -e "\e[0;36m Скрипта работи за следните AUR помощници: \e[0m"
echo -e "\e[4;32m aura pacaur pakku paru pikaur trizen yay \e[0m"
echo

# За aura помощник
if [[ `pacman -Qq aura` == aura ]]
  then
    echo
    echo -e "\e[4;36m Намерен е aura помощник \e[0m"
    aura -Au
    echo
  # За pacaur помощник
  elif [[ `pacman -Qq pacaur` == pacaur ]]
    then
      echo
      echo -e "\e[4;36m Намерен е pacaur помощник \e[0m"
      pacaur -Syua --noconfirm --noedit
      echo
  # За pakku-git помощник
  elif [[ `pacman -Qq pakku-git` == pakku-git ]]
    then
      echo
      echo -e "\e[4;36m Намерен е pakku помощник \e[0m"
      pakku -Sz --noconfirm --noedit
      echo
  # За paru помощник
  elif [[ `pacman -Qq paru` == paru ]]
    then
      echo
      echo -e "\e[4;36m Намерен е paru помощник \e[0m"
      paru -Sua
      echo
  # За pikaur помощник
  elif [[ `pacman -Qq pikaur` == pikaur ]]
    then
      echo
      echo -e "\e[4;36m Намерен е pikaur помощник \e[0m"
      pikaur -Sua
      echo
  # За trizen помощник
  elif [[ `pacman -Qq trizen` == trizen ]]
    then
      echo
      echo -e "\e[4;36m Намерен е trizen помощник \e[0m"
      trizen -Syua --noconfirm --noedit
      echo
  # За yay помощник
  elif [[ `pacman -Q yay | awk '{print $1}'` == yay ]]
    then
      echo
      echo -e "\e[4;36m Намерен е yay помощник \e[0m"
      yay -Syu
      echo
  # При несъвпадение
  else 
    echo
    echo -e "\e[4;32m Трабва да инсталирате AUR помощник \e[0m"
    echo
fi
