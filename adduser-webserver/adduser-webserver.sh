#!/bin/bash
# Automation script for creating user with all necessary directories and configurations for php-fpm pools
# Author: Rosen Aleksandrov rosen@aleksandrov.tech
# At the moment is possible to add user just for one php version, to add another specific version to existing user, just rerun the script with selected php version.
# LICENSE: GPL

echo "Welcome to $0"
echo

# checking for root
if ! [ "$(id -u)" = 0 ]; then
    echo -e "\e[1;31m $0 must be executed as root. \e[0m\n"
    exit 1
else
    echo -e "\e[1;32m $0 executed as root. \e[0m\n"
fi

# functions
if [ $# -eq 0 ]; then
    echo -e "\e[1;31m No parameters provided. Example $0 -p 73 or $0 --php 73 \e[0m\n"
    exit 1
fi

while [[ "$#" -gt 0 ]]; do
    case $1 in
	-p|--php) fpmver="$2"; shift ;;
	*) echo -e "\e[1;31m Unknown parameter passed: $1 . Example $0 -p 73 or $0 --php 73 \e[0m\n"; exit 1 ;;
    esac
    shift
done

echo -e "\e[1;36m Selected version: ${fpmver} \e[0m\n"

#
#Work to do....
#

read -p "Please provide username. `echo $'\n--> '`" username

# fpm template
fpmtemplate="
[$username]
user = "$username"
group = "$username"
listen = /var/run/$username-fpm${fpmver}.sock
listen.owner = "$username"
listen.group = "$username"
listen.mode = 0666
listen.allowed_clients = 127.0.0.1
pm = dynamic
pm.max_children = 50
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 35
slowlog = /home/$username/logs/www-slow.log
env[TMP] = /home/$username/tmp
env[TMPDIR] = /home/$username/tmp
env[TEMP] = /home/$username/tmp
php_admin_value[error_log] = /home/"$username"/logs/"$username"-fpm${fpmver}.log
php_admin_flag[log_errors] = on
php_admin_value[memory_limit] = 512M
php_value[session.save_handler] = files
php_value[session.save_path]    = /home/"$username"/php${fpmver}/session
php_value[soap.wsdl_cache_dir]  = /home/"$username"/php${fpmver}/wsdlcache
php_value[opcache.file_cache]  = /home/"$username"/php${fpmver}/opcache
"


# check if user exist.
# if user not exist on system, it will be created with all directories needed for.

if id -u ${username} &>> /dev/null; then echo -e "\e[1;38m user $username already exists, moving to fpm-pool configuration.";
else

    # username creation

    read -e -p "User ${username} does not exist, would you like to create? [y/n]: " -i "y" choice
    case "$choice" in
	y) useradd -m "${username}"; ;;
	n) exit 1; ;;
	*) echo -e "\e[1;31m Invalid, please answer with y/n. \e[0m"; exit 1; ;;
    esac



    # user dirs creation

    read -e -p "Would you like to create user directories php56 php71 php73 publick_html ssl and tmp? [y/n]: " -i "y" userdirs
    case "$userdirs" in
	y) mkdir -p /home/$username/{php56,php71,php73}/{opcache,session,wsdlcache} /home/$username/{logs,public_html,ssl,tmp}; ;;
	n) exit 1; ;;
	*) echo -e "\e[1;31m Invalid, please answer with y/n. \e[0m"; exit 1; ;;
    esac
fi

# template generator

read -e -p "Would you like to generate php-fpm configuration file? [y/n]: " -i "y" phpfpm
case "$phpfpm" in
    y) echo -e "\e[1;33m generating php-fpm configuration for user ${username} \e[0m"; echo -e "$fpmtemplate \n" > /etc/opt/remi/php${fpmver}/php-fpm.d/$username.conf; ;;
    n) exit 1; ;;
    *) echo -e "\e[1;31m Invalid, please answer with y/n. \e[0m"; exit 1; ;;
esac
