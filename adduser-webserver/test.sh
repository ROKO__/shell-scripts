#!/bin/bash

# variables
username="ecoblaze"
fpmver="56"

# fpm template
fpmtemplate="
[$username]
user = $username
group = $username
listen = /var/run/$username-fpm${fpmver}.sock
listen.owner = $username
listen.group = $username
listen.mode = 0666
listen.allowed_clients = 127.0.0.1
pm = dynamic
pm.max_children = 50
pm.start_servers = 5
pm.min_spare_servers = 5
pm.max_spare_servers = 35
slowlog = /home/$username/logs/www-slow.log
env[TMP] = /home/$username/tmp
env[TMPDIR] = /home/$username/tmp
env[TEMP] = /home/$username/tmp
php_admin_value[error_log] = /home/$username/logs/$username-fpm${fpmver}.log
php_admin_flag[log_errors] = on
php_admin_value[memory_limit] = 512M
php_value[session.save_handler] = files
php_value[session.save_path]    = /home/$username/php${fpmver}/session
php_value[soap.wsdl_cache_dir]  = /home/$username/php${fpmver}/wsdlcache
php_value[opcache.file_cache]  = /home/$username/php${fpmver}/opcache
"
# template generator
#for i in $fpmtemplate; do
#	 echo "$i" > test.conf
#      done

echo -e "$fpmtemplate \n" > /etc/opt/remi/php${fpmver}/php-fpm.d/$username.conf
